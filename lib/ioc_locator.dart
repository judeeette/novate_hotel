// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:ioc/ioc.dart';
import 'package:novate_hotel_app/app_config.dart';
import 'package:novate_hotel_app/http_client_config.dart';
import 'package:novate_hotel_app/services/user_api.dart';



void iocLocator(AppConfig config) {
  Ioc().bind('userApi', (ioc) => UserApi(config: config, client: initHttpClient()));
}