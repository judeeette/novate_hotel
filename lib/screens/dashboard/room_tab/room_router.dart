import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_start_date.dart';

import '../router_page.dart';

class RoomRouter extends RouterPage {
  RoomRouter() : super(navigatorKey: GlobalKey<NavigatorState>());

  @override
  PageRoute generateRoute(RouteSettings settings) {
    return MaterialPageRoute<void>(
      settings: settings,
      builder: (BuildContext context) {
        switch (settings.name) {
          case '/':
            return const BookingRoomStartDate();
          default:
            return const BookingRoomStartDate();
        }
      },
    );
  }
}
