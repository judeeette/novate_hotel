import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/room_router.dart';
import 'package:novate_hotel_app/screens/dashboard/router_page.dart';
import 'package:novate_hotel_app/screens/dashboard/home_tab/home_router.dart';


class BottomTabNavigator extends StatefulWidget {
  const BottomTabNavigator({Key? key}) : super(key: key);

  @override
  BottomTabNavigatorState createState() => BottomTabNavigatorState();
}

class BottomTabNavigatorState extends State<BottomTabNavigator> {
  int _selectedIndex = 0;
  bool _bottomTabNavigatorVisible = true;
  static const activeColor = colorDarkBlue;
  static const normalColor = colorDarkGray;

  hideBottomTabNavigator() {
    setState(() {
      _bottomTabNavigatorVisible = false;
    });
  }

  showBottomTabNavigator() {
    setState(() {
      _bottomTabNavigatorVisible = true;
    });
  }

  final List _widgetOptions = <dynamic>[
    HomeRouter(),
    RoomRouter(),
    null,
    null,
    // TransferRouter(),
    // SupportScreen(),
    // AccountRouter()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  Future<bool> _onBackPressed() async {
    if (_widgetOptions[_selectedIndex] is RouterPage) {
      final RouterPage page = _widgetOptions[_selectedIndex] as RouterPage;
      return !await page.navigatorKey.currentState!.maybePop();
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            body: Center(
              child: _widgetOptions.elementAt(_selectedIndex),
            ),
            bottomNavigationBar: _bottomTabNavigatorVisible
                ? BottomNavigationBar(
                    type: BottomNavigationBarType.fixed,
                    backgroundColor: Colors.white,
                    showUnselectedLabels: false,
                    items: <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        activeIcon: SvgPicture.asset(
                          'lib/assets/svgs/home_icon.svg',
                          color: activeColor,
                          width: 30,
                          height: 30,
                        ),
                        icon: SvgPicture.asset(
                          'lib/assets/svgs/home_icon.svg',
                          color: normalColor,
                          width: 30,
                          height: 30,
                        ),
                        label: '',
                      ),
                      BottomNavigationBarItem(
                        activeIcon: SvgPicture.asset(
                          'lib/assets/svgs/bed_icon.svg',
                          color: activeColor,
                          width: 20,
                          height: 20,
                        ),
                        icon: SvgPicture.asset(
                          'lib/assets/svgs/bed_icon.svg',
                          color: normalColor,
                          width: 20,
                          height: 20,
                        ),
                        label: '',
                      ),
                      BottomNavigationBarItem(
                        activeIcon: SvgPicture.asset(
                          'lib/assets/svgs/comment_icon.svg',
                          color: activeColor,
                          width: 20,
                          height: 20,
                        ),
                        icon: SvgPicture.asset(
                          'lib/assets/svgs/comment_icon.svg',
                          color: normalColor,
                          width: 20,
                          height: 20,
                        ),
                        label: '',
                      ),
                      BottomNavigationBarItem(
                        activeIcon: SvgPicture.asset(
                          'lib/assets/svgs/user_icon.svg',
                          color: activeColor,
                          width: 20,
                          height: 20,
                        ),
                        icon: SvgPicture.asset(
                          'lib/assets/svgs/user_icon.svg',
                          color: normalColor,
                          width: 20,
                          height: 20,
                        ),
                        label: '',
                      ),
                    ],
                    currentIndex: _selectedIndex,
                    selectedItemColor: activeColor,
                    unselectedItemColor: normalColor,
                    onTap: _onItemTapped,
                  )
                : null));
  }
}
