import 'package:flutter/material.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';

Widget MainLogo() {
  return Column(
    children: <Widget>[
      Image.asset(
        hotelLogo,
        fit: BoxFit.contain,
      ),
      const Text(
        'NovHôtel',
        style: TextStyle(
            color: Colors.white, fontSize: 51),
      )
    ],
  );
}
