import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/regex.dart';

Widget InputField(
    {required String initialValue,
    required String labelText,
    String? label,
    required TextInputType inputType,
    dynamic onChanged,
    bool obscureText = false,
    String? valueToCheck}) {
  return Container(
      height: 93,
      child: Column(
        children: <Widget>[
          label != null
              ? Container(
                  margin: const EdgeInsets.only(bottom: 2, left: 10),
                  alignment: Alignment.topLeft,
                  child: Text(
                    label,
                    style: const TextStyle(fontSize: 18),
                  ),
                )
              : Container(),
          TextFormField(
            obscureText: obscureText,
            initialValue: initialValue,
            onChanged: onChanged,
            validator: (value) {
              if (value!.isEmpty) {
                return '*Requis!';
              } else if (inputType == TextInputType.emailAddress &&
                  !emailRegEx.hasMatch(value)) {
                return '*Adresse e-mail invalide!';
              } else if (obscureText == true &&
                  valueToCheck != null &&
                  value != valueToCheck) {
                return 'Les deux mots mot de passes sont différents!';
              }
              return null;
            },
            decoration: InputDecoration(
              hintText: labelText,
              // labelText: labelText,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              errorBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: colorRed)),
              labelStyle: const TextStyle(fontSize: 16),
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              contentPadding:
                  const EdgeInsets.only(bottom: 0, top: 0, left: 15),
            ),
          ),
        ],
      ));
}
