import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:novate_hotel_app/app_config.dart';
import 'package:novate_hotel_app/exceptions/user_exception.dart';
import 'package:novate_hotel_app/models/responses/register-user-response/register_user_response.dart';
import 'package:novate_hotel_app/models/schemas/user/user.dart';

class UserApi {
  final AppConfig config;
  final http.Client client;
  final String endpoint = '/users';

  UserApi({required this.config, required this.client});

  Future<RegisterUserResponse> registerUser(User user) async {
    final String url = config.apiBaseUrl + endpoint;
    final body = jsonEncode({
      'email': user.email,
      'password': user.password,
      'lastname': user.lastname,
      'firstname': user.firstname,
      'phone': user.phone
    });
    final response = await client.post(Uri.parse(url),
        headers: {
          HttpHeaders.acceptHeader: 'application/json',
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: body);
        
    Map<String, dynamic> responseMap = jsonDecode(response.body);
    final userResponse = RegisterUserResponse.fromJson(responseMap);
    if (response.statusCode != 201) {
      throw const UserException(
          "Unable to create user at this time, please try again later");
    }
    return userResponse;
  }
}
